export const styles = theme => ({
  container: {
    height: 30,
    background: "#fff",
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    padding: "0 10px"
  },
  Dowcontainer: {
    borderBottomStyle: "solid",
    borderBottomColor: "#bc2132",
    height: 35,
    background: "#fff",
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    width: "100%",
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    position: "fixed",
    bottom: 24,
    zIndex: 9
  },
  driv: {
    position: "relative",
    top: -2,
    fontSize: 10
  },
  text: {
    fontSize: 10
  },
  textDown: {
    position: "relative",
    marginRight: 5,
    top: 2,
    fontSize: 10
  },
  textAdd: {
    fontSize: 13
  },
  img: {
    width: 40
  },
  imgDow: {
    width: 15
  },
  logoDisco: {
    width: 65,
    marginLeft: 5
  }
});

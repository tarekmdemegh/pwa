/*
 *  Copyright (c) 2019 CHIFCO
 * <> with ❤ by Aymen ZAOUALI
 *
 * The computer program contained herein contains proprietary
 * information which is the property of Chifco.
 * The program may be used and/or copied only with the written
 * permission of Chifco or in accordance with the
 * terms and conditions stipulated in the agreement/contract under
 * which the programs have been supplied.
 *
 * Created on Wed Apr 24 2019
 */

import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "./style";
import DownloadApp from "./downloadApp";

class Footer extends Component {
  render() {
    return (
      <div id="footer">
        <DownloadApp
          condition={this.props.condition}
          onClick={this.props.onClick}
        />
      </div>
    );
  }
}

export default withStyles(styles)(Footer);

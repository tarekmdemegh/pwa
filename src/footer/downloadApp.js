import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import { styles } from "./style";

class DownloadApp extends Component {
  render() {
    const { classes } = this.props;

    return (
      <div className={classes.Dowcontainer} onClick={this.props.onClick}>
        <div
          style={{ display: "flex", alignItems: "center", padding: "0 10px" }}
        >
          <Typography component="p" className={classes.textAdd}>
            AJOUTEZ L’EXTENTION
          </Typography>
        </div>

        <div
          style={{ display: "flex", alignItems: "center", padding: "0 10px" }}
        >
          <Typography component="p" className={classes.textDown}>
            INSTALLER
          </Typography>
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(DownloadApp);

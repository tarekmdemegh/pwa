import React, { Component, Suspense, lazy } from "react";
import Footer from "./footer";
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      installButton: null,
      challengeClick: false,
    };
  }

  installPrompt = null;
  componentDidMount() {
    //console.log(localStorage.getItem("challenge"))

    // console.log('app.js');
    // messaging.requestPermission()
    //   .then(async function () {
    //     const token = await messaging.getToken();
    //     console.log('toekn', token);

    //   })
    //   .catch(function (err) {
    //     console.log("Unable to get permission to notify.", err);
    //   });
    // navigator.serviceWorker.addEventListener("message", (message) => console.log(message));

    // console.log("Listening for Install prompt");
    window.addEventListener("beforeinstallprompt", (e) => {
      // For older browsers
      e.preventDefault();
      // console.log("Install Prompt fired");
      this.installPrompt = e;
      // See if the app is already installed, in that case, do nothing
      if (
        (window.matchMedia &&
          window.matchMedia("(display-mode: standalone)").matches) ||
        window.navigator.standalone === true
      ) {
        this.setState({
          installButton: false,
        });
      }
      // Set the state variable to make button visible
      this.setState({
        installButton: true,
      });
    });
  }

  installApp = async () => {
    if (!this.installPrompt) return false;
    this.installPrompt.prompt();
    // Remove the event reference
    this.installPrompt = null;
    // Hide the button
    this.setState({
      installButton: false,
    });
  };

  clickChallenge = async () => {
    //console.log("close")
    localStorage.setItem("clicked", true);
    this.setState({
      challengeClick: false,
    });
  };

  render() {
    const { installButton, challengeClick } = this.state;

    return (
      <>
        {installButton && (
          <Footer
            condition={this.state.installButton}
            onClick={this.installApp}
          />
        )}
      </>
    );
  }
}

export default App;
